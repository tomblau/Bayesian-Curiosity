# Methodology

This is a description of how to reproduce the experiments in the paper. It only pertains to the simulation experiments, as reproducing physical experiments is a bitch and a half. We will use vrep experiments as the canonical example. Note that the vrep scene `scenes/rl_lowd_reaching.ttt` must be running for the following examples to work.

## Vanilla RL

- Run one of the experiments in `examples` while leaving the `init_weights` line commented out. This will create a new policy and train it from scratch.

- If you want start with a specific policy initialisation, you can create one following the template seen in the experiment files. Create a policy (e.g. `GaussianMLPPolicy`) object and baseline (e.g. `GaussianMLPBaseline`) object. To save them:

````
import joblib
joblib.dump(dict(policy=policy, baseline=baseline), "/path/to/destination/file.pkl")
````

where `policy` and `baseline` are the policy and baseline objects you created. You can choose whatever filepath you want to save your model, but it has to be a `.pkl` file. Also, a .pkl saved in python 2 probably can't be loaded in python 3 and vice versa.

For convenience, a randomly initialised example policy for low-dimensional reaching is available at `data/local/experiment/networks/untrained_lowd_reaching1.pkl`.

## Pre-training

You can pre-train your policy with expert demonstrations using `Bayesian-Curiosity/rllab/envs/vrep/supervised.py`.
You must replace the strings "/path/to/source/file.pkl", "/path/to/dest/file.pkl" and "/path/to/data" with appropriate filepaths. Training data for the low-dimensional reaching and grasping tasks is available in `Bayesian-Curiosity/rllab/envs/vrep/data/lowd_half` and `Bayesian-Curiosity/rllab/envs/vrep/grasping/data/lowd_anneal` respectively. 

A pre-trained policy for low-dimensional reaching is available at `Bayesian-Curiosity/data/local/experiment/networks/trained_lowd_reaching1.pkl`

## BLR

- Make a BLRPolicy out of a GaussianMLPPolicy:

````
from rllab.policies.blr_policy import BLRPolicy
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import IdentityKernel, QMCRFFKernel
from rllab.envs.env_spec import EnvSpec
from rllab.spaces import Box
import joblib
import numpy as np
import lasagne.layers as L
import lasagne.nonlinearities as NL	

feature_dim = 5
output_dim = 6
num_rff = 100
idx = 1
m = num_rff * feature_dim
src = "/Bayesian-Curiosity//data/local/experiment/networks/untrained_lowd_reaching%d.pkl" %idx
dest = "/Bayesian-Curiosity//data/local/experiment/networks/untrained_lowdreaching_rffblrp%d.pkl" %idx
data = joblib.load(src)
nw = data['policy']._mean_network
nw._l_out = nw._l_out.input_layer
nw._l_out.nonlinearity = NL.identity
reg = BLR(2*m, output_dim)
kernel = QMCRFFKernel(M=m, d=feature_dim)
#reg = BLR(feature_dim, output_dim)
#kernel = IdentityKernel()
obspace = Box(low=0., high=1., shape=(9,))
bound = np.ones(6) * np.pi / 5
acspace = Box(-bound, bound)
env = EnvSpec(obspace, acspace)
pi = BLRPolicy(env, reg, kernel, feature_dim=feature_dim, network=nw)
joblib.dump(dict(policy=pi), dest)
````

- Train the BLRPolicy's feature network using `blr_supervised.py`. The variable `src` must point to the filepath of the policy you wish to train.

- Make a VisitationGaussianPolicy out of a trained GaussianMLPPolicy and trained BLRPolicy:

````
from rllab.policies.visitation_gaussian_policy import VisitationGaussianPolicy
import joblib
import numpy as np

idx = 1
data = joblib.load("/Bayesian-Curiosity//data/local/experiment/networks/trained_lowd_reaching%d.pkl" %idx)
d2 = joblib.load("/Bayesian-Curiosity//data/local/experiment/networks/trained_lowreaching_rffblrp%d.pkl" %idx)
mn = data['policy']._mean_network
sn = data['policy']._std_network
n2 = d2['policy']._network
r = d2['policy']._regression
k = d2['policy']._kernel
es =  data['policy']._Serializable__args[0]
VBLRP = VisitationGaussianPolicy(es, mn, sn, n2, r, k)
joblib.dump(dict(policy=VBLRP, baseline=data['baseline']),
	"/Bayesian-Curiosity//data/local/experiment/networks/VisitationLowDReachingPolicy%d.pkl" %idx)
````

- Train with TRPO using the keyword argument `sampler_cls=VisitationBatchSampler` e.g. by running `/Bayesian-Curiosity//examples/visitation/lowd_reaching_ex.py`

## RND

- Make a target_network and predictor_network of the same architecture as a GaussianMLPPolicy's mean_network

````
from rllab.core.network import AltRoboNetwork as RoboNetwork
import lasagne.nonlinearities as NL

rep_size = 256
obs_shape = (256 ,256, 4)
predictor_network = RoboNetwork(
    input_shape=obs_shape,
    output_dim=rep_size,
    hidden_sizes=(256, 256),
    pool_sizes=(2, 2, 2, 2),
    conv_filters=(32, 64, 64, 64),
    conv_filter_sizes=(5, 5, 3, 3),
    conv_strides=(2, 2, 1, 1),
    conv_pads=(2, 2, 1, 1),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.identity,
)
target_network = RoboNetwork(
    input_shape=obs_shape,
    input_layer=predictor_network.input_layer,
    output_dim=rep_size,
    hidden_sizes=(256, 256),
    pool_sizes=(2, 2, 2, 2),
    conv_filters=(32, 64, 64, 64),
    conv_filter_sizes=(5, 5, 3, 3),
    conv_strides=(2, 2, 1, 1),
    conv_pads=(2, 2, 1, 1),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.identity,
)
````

- LowD Case:

````
import lasagne.nonlinearities as NL
from rllab.core.network import MLP

rep_size=256
obs_shape = (9,)
predictor_network = MLP(
    input_shape=obs_shape,
    output_dim=rep_size,
    hidden_sizes=(256, 256),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.identity,
)

target_network = MLP(
    input_shape=obs_shape,
    input_layer=predictor_network.input_layer,
    output_dim=rep_size,
    hidden_sizes=(256, 256),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.identity,
)
````

- Make an RNDGaussianPolicy out of a GaussianMLPPolicy (using the appropriate pre-trained policy) and two networks

````
from rllab.policies.rnd_gaussian_policy import RNDGaussianPolicy
from rllab.envs.env_spec import EnvSpec
from rllab.spaces import Box
import numpy as np
import joblib

idx = 1
src = "/Bayesian-Curiosity//data/local/experiment/networks/trained_lowd_reaching%d.pkl" %idx
dest = "/Bayesian-Curiosity//data/local/experiment/networks/RNDLowDReachingPolicy%d.pkl" %idx
data = joblib.load(src)
mean_network = data['policy']._mean_network
std_network = data['policy']._std_network
obspace = Box(low=0., high=1., shape=(9,))
bound = np.ones(6) * np.pi / 5
acspace = Box(-bound, bound)
env = EnvSpec(obspace, acspace)

pi = RNDGaussianPolicy(env, mean_network, std_network, target_network, predictor_network)
joblib.dump(dict(policy=pi, baseline=data['baseline']), dest)
````

- Train with TRPO using the keyword argument `sampler_cls=RNDBatchSampler` e.g. by running `Bayesian-Curiosity//examples/rnd/lowd_reaching_ex.py`


## VIME

To run VIME experiments, you will have to install the code provided by the original paper's authors. This is easily done by going to the root folder and running:

````
git submodule add -f git@github.com:openai/vime.git sandbox/vime
touch sandbox/__init__.py
````