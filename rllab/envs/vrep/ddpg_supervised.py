import lasagne
import lasagne.regularization as R
import lasagne.layers as L
import theano
import theano.tensor as T
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 400

def var_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reg(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def res_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reset(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def network_noise(net, train_clip=False, thresh=3):
	return T.mean([layer.log_sigma2.mean()
					for layer in lasagne.layers.get_all_layers(net) if 'log_sigma2' in layer.__dict__])

def iterate_minibatches(obs, act, qs, batchsize, shuffle=False):
	assert len(obs) == len(qs)
	indices = np.arange(len(qs))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield obs[excerpt], act[excerpt], qs[excerpt]

	

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	qs = np.load(folder + 'qs.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)
	qs = np.concatenate([qs[entry] for entry in qs.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, actions, qs

def print_weights(network):
	for l in L.get_all_layers(network):
		if 'W' in l.__dict__:
			print(np.abs(l.W.get_value()).mean())

if __name__ == '__main__':
	print("prepare network")

	obs_var = T.matrix('obs')
	act_var = T.matrix('act')
	target = T.matrix('targets')
	src = "/rllab/data/local/experiment/ddpg/trained_qf.pkl"
	data = joblib.load(src)
	qf = data['qf']
	print("prepare data")
	obs, act, qs = load_data('data/demo_')
	for batch in iterate_minibatches(obs, act, qs, 200, shuffle=True):
		o_inp, a_inp, targets = batch
	print("prepare training")
	qf_output = qf._output_layer

	qf_pred = lasagne.layers.get_output(qf_output, deterministic=False, train_clip=True)
	qf_pred_error = lasagne.objectives.squared_error(qf_pred, target).mean()
	qf_variational_regularization = var_reg(qf_output)
	loss = qf_pred_error \
			+ 1 * qf_variational_regularization
	qf_params = lasagne.layers.get_all_params(qf_output, trainable=True)
	qf_updates = lasagne.updates.adam(loss, qf_params)
	print("compile std functions")
	train_fn = theano.function([qf._obs_layer.input_var, qf._action_layer.input_var, target], loss, updates=qf_updates)
	reg_fn = theano.function([], qf_variational_regularization)
	eval_fn = theano.function([qf._obs_layer.input_var, qf._action_layer.input_var, target], qf_pred_error)
	#larray = dist.log_likelihood_array(target, dist_info)
	#eval_fn = theano.function([input_var, target], larray.mean(axis=0))
	for epoch in range(num_epochs):
		train_err = 0
		eval_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(obs, act, qs, 200, shuffle=True):
			o_inp, a_inp, targets = batch
			err = train_fn(o_inp, a_inp, targets)
			#print("train_err = %f" %err)
			train_err += err
			#eval_err += eval_fn(inputs, targets)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		#print("\teval err:\t\t{}".format(eval_err / train_batches))
		print("\tvariational regularization loss:\t{}".format(reg_fn()))
		print("\ttraining loss_0:\t\t{:.6f}".format(train_err / train_batches))

	err= 0.
	batches = 0
	for batch in iterate_minibatches(obs, act, qs, 200, shuffle=False):
		o_inp, a_inp, targets = batch
		err += eval_fn(o_inp, a_inp, targets)
		batches += 1
	print("\teval error:\t{}".format(err/batches))
	print("save policy")

	joblib.dump(data, "/rllab/data/local/experiment/ddpg/trained_qf.pkl")
