import lasagne
import lasagne.regularization as R
import theano
import theano.tensor as T
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 200

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]


def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	#joints = np.load(folder + 'backup/demo_joints.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, actions

if __name__ == '__main__':
	print("prepare data")
	folder = str()

	print("prepare network")

	input_var = T.matrix('inputs')
	target = T.matrix('targets')
	src = "/rllab/data/local/experiment/experiment_2018_07_27_00_38_38_0001/itr_0.pkl"
	data = joblib.load(src)
	policy = data['policy']
	X,y = load_data('data/half/demo_')
	X = X[:8000]
	y = y[:8000]
	
	dist = policy.distribution
	dist_info = policy.dist_info_sym(input_var)
	likelihood = dist.log_likelihood_sym(target, dist_info)
	policy_outputs = [policy._l_mean, policy._l_log_std]
	prediction = lasagne.layers.get_output(policy_outputs[-1],input_var)
	#loss = T.abs_(prediction - target).mean()
	loss = -likelihood.mean()
	loss = loss + 1e-4*R.regularize_network_params(policy_outputs[-1], R.l2)
	params = lasagne.layers.get_all_params(policy_outputs[-1], trainable=True)
	updates = lasagne.updates.adam(loss, params)
	train_fn = theano.function([input_var, target], loss, updates=updates)
	eval_fn = theano.function([input_var], prediction[:, :-1])
	print("begin training...")
	for epoch in range(num_epochs):
		if epoch % 50 == 0:
			joblib.dump(data, "/rllab/data/local/experiment/networks/trained_reach1.pkl")
		train_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets = batch
			train_err += train_fn(inputs, targets)
			res = eval_fn(inputs)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
	
	print("save policy")
	joblib.dump(data, "/rllab/data/local/experiment/networks/trained_reach1.pkl")

	sys.exit()
	
	uncertainties = []
	for batch in iterate_minibatches(X, y, 200):
		inputs, _ = batch
		predictions = np.array([eval_fn(inputs) for _ in range(10)])
		uncertainties.append(predictions.std(axis=0))
	uncertainties = np.concatenate(np.array(uncertainties))
	np.savez_compressed("uncertainties.npz", uncertainties)

	y = np.log(np.load("uncertainties.npz")['arr_0'])
	print(len(X), len(y))

#	sys.exit()

	data, old_policy, policy, old_input = load_policy("/rllab/data/local/experiment/experiment_2017_05_24_06_50_20_0001/trained_dropout.pkl", input_var, "variance")
	prediction = lasagne.layers.get_output(policy)
	loss = T.abs_(prediction - target).mean()
	loss = loss + 1e-4*R.regularize_network_params(policy, R.l2)
	params = lasagne.layers.get_all_params(policy, trainable=True)
	updates = lasagne.updates.adam(loss, params)
	train_fn = theano.function([input_var, target], loss, updates=updates)
	eval_fn = theano.function([input_var], prediction)
	for epoch in range(num_epochs):
		train_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets = batch
			train_err += train_fn(inputs, targets)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))

	lasagne.layers.get_all_layers(policy)[0].input_var = old_input
	data["policy"]._l_log_std = policy
	joblib.dump(data, "/rllab/data/local/experiment/experiment_2017_05_24_06_50_20_0001/trained_dropout_uncertainty.pkl")
