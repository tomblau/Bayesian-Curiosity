import lasagne
import lasagne.regularization as R
import lasagne.layers as L
import theano
import theano.tensor as T
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 400

def var_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reg(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def res_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reset(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def network_noise(net, train_clip=False, thresh=3):
	return T.mean([layer.log_sigma2.mean()
					for layer in lasagne.layers.get_all_layers(net) if 'log_sigma2' in layer.__dict__])

def weight_layers(net):
	layers = L.get_all_layers(net)
	fctype = type(layers[-1])
	weights = {}
	for l in layers:
		if type(l) == fctype:
			weights[l] = 1e-4
	return weights

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt], excerpt

	

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	#stds = np.load(folder + 'diff.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)
	#stds = np.concatenate([stds[entry] for entry in stds.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, actions#, stds

def print_weights(network):
	for l in L.get_all_layers(network):
		if 'W' in l.__dict__:
			print(np.abs(l.W.get_value()).mean())

if __name__ == '__main__':
	print("prepare network")

	input_var = T.matrix('inputs')
	target = T.matrix('targets')
	src = "/rllab/data/local/experiment/experiment_2018_01_29_05_24_54_0001/itr_0.pkl"
	data_0 = joblib.load(src)
	data_1 = joblib.load(src)
	policy_0 = data_0['policy']
	policy_1 = data_1['policy']
	print("prepare data")
	obs_0, act_0 = load_data('data/half/demo_')
	obs_1, act_1 = load_data('data/half/rdemo_')
	divider = 8000
	X_0 = obs_0[:divider]
	y_0 = act_0[:divider]
	X_1 = obs_1[:divider]
	y_1 = act_1[:divider]
	print("prepare training")
	dist_0 = policy_0.distribution
	dist_1 = policy_1.distribution
	dist_info_0 = policy_0.dist_info_sym(input_var, deterministic=False, train_clip=True)
	dist_info_1 = policy_1.dist_info_sym(input_var, deterministic=False, train_clip=True)
	likelihood_0 = dist_0.log_likelihood_sym(target, dist_info_0)
	likelihood_1 = dist_1.log_likelihood_sym(target, dist_info_1)
	policy_outputs_0 = [policy_0._l_mean, policy_0._l_log_std]
	policy_outputs_1 = [policy_1._l_mean, policy_1._l_log_std]
	pred = lasagne.layers.get_output(policy_outputs_0[0], input_var, deterministic=True)
	pred_error = lasagne.objectives.squared_error(pred, target)
	#layer_weights = weight_layers(policy_outputs[0])
	#l2_regularization = R.regularize_layer_params_weighted(layer_weights, R.l2)
	#sig_noise = network_noise(policy_outputs[0])
	print("prepare objective function")
	loss_0 = -likelihood_0.mean() \
				+ 1 * var_reg(policy_outputs_0[0])
	loss_1 = -likelihood_1.mean() \
				+ 1 * var_reg(policy_outputs_1[1])
	#pred = lasagne.layers.get_output(policy_outputs[0], input_var)
	#noise = lasagne.layers.get_output(policy_outputs[1], input_var)
	#pred_error = lasagne.objectives.squared_error(pred, target)
	#log_error = T.log(pred_error)
	params_0 = lasagne.layers.get_all_params(policy_outputs_0[0])
	params_1 = lasagne.layers.get_all_params(policy_outputs_1[1])
	updates_0 = lasagne.updates.adam(loss_0, params_0)
	updates_1 = lasagne.updates.adam(loss_1, params_1)
	print("prepare training function")
	train_fn_0 = theano.function([input_var, target], loss_0, updates=updates_0)
	train_fn_1 = theano.function([input_var, target], loss_1, updates=updates_1)
	reg_fn = theano.function([], var_reg(policy_outputs_0))
	print("prepare evaluation function")
	eval_fn = theano.function([input_var, target], pred_error.mean())
	print("begin training...")
	for epoch in range(num_epochs):
		if epoch % 50 == 0:
			print_weights(policy_outputs_0[0])
			#joblib.dump(data_0, "/rllab/data/local/experiment/networks/trained_vd_simultaneous.pkl")
		train_err = 0
		eval_err = 0
		train_batches = 0
		uncertainties = 0
		start_time = time.time()
		iter_0 = iterate_minibatches(X_0, y_0, 200, shuffle=True)
		iter_1 = iterate_minibatches(X_1, y_1, 200, shuffle=True)
		batch_0 = next(iter_0, None)
		batch_1 = next(iter_1, None)
		while batch_0 is not None or batch_1 is not None:
			if batch_0 is not None:
				inputs_0, targets_0, _ = batch_0
				train_err += train_fn_0(inputs_0, targets_0)
				eval_err += eval_fn(inputs_0, targets_0)
				batch_0 = next(iter_0, None)
			if batch_1 is not None:
				inputs_1, targets_1, _ = batch_1
				train_err += train_fn_1(inputs_1, targets_1)
				batch_1 = next(iter_1, None)
			train_batches += 1
	
		L.set_all_param_values(
								policy_outputs_1[0],
								L.get_all_param_values(policy_outputs_0[0])
								)
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		print("\tvariational regularization loss:\t{}".format(reg_fn()))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
		print("\tpred error:\t\t{}".format(eval_err / train_batches))
	print_weights(policy_outputs_0[0])
	L.set_all_param_values(
								policy_outputs_0[1],
								L.get_all_param_values(policy_outputs_1[1])
								)
	joblib.dump(data_0, "/rllab/data/local/experiment/networks/trained_vd_simultaneous4.pkl")
	sys.exit()
