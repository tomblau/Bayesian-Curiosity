from rllab import spaces
from rllab.envs.base import PropertyEnv
import rllab.envs.vrep.vrep as vrep
from math import pi
import time
import sys
import numpy as np
from cached_property import cached_property

PRETRAIN_LENGTH = 0
PRETRAIN_START = 0
PRETRAIN_END = PRETRAIN_START + PRETRAIN_LENGTH
N_JOINTS = 6
RESOLUTION = 128
N_CHANNELS = 4
XBOUNDS = (-0.565, -0.165)
YBOUNDS = (-0.2, 0.19)
Z = 0.25
CYLINDER_RADIUS=0.03
CYLINDER_H=0.1
JOINT_POSITION_CODE = 15
OBJECT_POSITION_CODE = 3
DEFAULT_JOINT_POS = [0, pi, pi, pi, pi, pi]
OMPL_PREPARE = 0
GET_IK = 1
TIMEOUT_CODE = 2
UNBOUND_JOINTS = [0, 3, 4, 5]
ERROR_ACTION = np.array([-1])


def shift_config(config):
	"""shift the config so that the values of all unbounded joints
		are in the range [-pi,pi]"""
	res = config
	for idx in UNBOUND_JOINTS:
		res[idx] = np.mod(res[idx], 2*pi)
		if res[idx] > pi:
			res[idx] = res[idx] - 2*pi
		if np.abs(res[idx]) > pi + 0.1:
			sys.exit("failed to shift vector element at index %d" %idx)
	return res

def find_nearest_goal(start, goal_set):
	min_goal = np.ones((N_JOINTS,))
	min_vector = np.ones((N_JOINTS,))*pi
	for goal in goal_set:
		goal = shift_config(np.mod(goal, 2*pi))
		vector = shift_config(goal - start)
		if distance_metric(vector) < distance_metric(min_vector):
			min_goal = goal
			min_vector = vector
	return min_goal, min_vector

def distance_metric(vector):
	"""
		The distance between two configs is the sum of angle differences
		excluding the final joint because it can take arbitrary values
	"""
	return np.max(np.abs(vector))

class JacoEnv(PropertyEnv):
	def __init__(self, goal_filepath="/rllab/rllab/envs/vrep/grasping/data/goals.npz", cup_filepath="/rllab/rllab/envs/vrep/grasping/data/cup_pos.npz"):
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print("connected to sim")
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		res = vrep.simxSynchronous(self.clientID,True)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")

		res, self.rgb_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_rgb",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorImage(self.clientID, self.rgb_sensor, 0, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire rgb sensor")
		res, self.depth_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_depth",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorDepthBuffer(self.clientID, self.depth_sensor, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire depth sensor")

		res, self.tip = vrep.simxGetObjectHandle(self.clientID, "Jaco_tip", vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire Jaco tip")
		res, self.iktarget = vrep.simxGetObjectHandle(self.clientID,"ikTarget",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cylinder",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		self.target_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1])
		vrep.simxSetObjectOrientation(self.clientID, self.iktarget, self.tip, (0,0,0), vrep.simx_opmode_oneshot)


		self.joints = [0 for _ in range(7)]
		self.joint_pos = np.zeros((N_JOINTS,))
		for i in range(1,N_JOINTS + 1):
			res, self.joints[i - 1] = vrep.simxGetObjectHandle(self.clientID, "Jaco_joint" + str(i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire joint %d" %i)
			res, self.joint_pos[i - 1] = vrep.simxGetJointPosition(self.clientID, self.joints[i - 1], vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire position for joint %d, error code: %d" %(i, res))

		res, self.joint_col = vrep.simxGetCollectionHandle(self.clientID, 'Joints', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire joint collection")
		vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)
		self.fingers = np.zeros((12), dtype=int)
		for i in range(12):
			res, self.fingers[i] = vrep.simxGetObjectHandle(self.clientID, "Dummy%d" %(11+i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire finger %d" %(i+11))
			vrep.simxGetObjectPosition(self.clientID, self.fingers[i], -1, vrep.simx_opmode_blocking)
		res, self.finger_col = vrep.simxGetCollectionHandle(self.clientID, 'Fingers', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire finger collection")
		vrep.simxGetObjectGroupData(self.clientID, self.finger_col, OBJECT_POSITION_CODE, vrep.simx_opmode_streaming)
		
		self.sim_time = 0
		self.goals = np.load(goal_filepath, encoding='bytes')['arr_0']
		self.positions = np.load(cup_filepath, encoding='bytes')['arr_0']
		assert self.goals.shape[0] == self.positions.shape[0]
		self.goal_idx = -1
		self.last_o = None

		self.distances = self.get_distances()

	def step(self, action):
		ik_succ, vector = self.get_vector()
		if ik_succ:
			action *= 0
		action += vector
		action = self.action_space.sample() 
		for i in range(N_JOINTS):
			#compute new joint pos after action
			self.joint_pos[i] += action[i]
			if self.joint_pos[i] < self.joint_space.low[i]:
				self.joint_pos[i] = self.joint_space.low[i]
			elif self.joint_pos[i] > self.joint_space.high[i]:
				self.joint_pos[i] = self.joint_space.high[i]
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		#simulate 0.5 seconds of movement. Should be enough to move pi/5 radians
		old_time = self.sim_time
		vrep.simxSynchronousTrigger(self.clientID)
		vrep.simxSynchronousTrigger(self.clientID)
		self.sim_time = self.get_time()
		#make sure the simulation actually updated
		while(old_time == self.sim_time):
			print("sleeping")
			vrep.simxSynchronousTrigger(self.clientID)
			time.sleep(0.01)
			self.sim_time = self.get_time()

		stuck = self.check_positions()
		observation = self.observe()
		if stuck > 0:
			reward = -100
			done = True
			if stuck == 2:
				done = True
			if done:
				print("failure!!!")
		else:
			self.distances = self.get_distances()
			vertical_dist = np.linalg.norm(self.distances[-1])
			horizontal_dist = np.linalg.norm(self.distances[:-1])
			total_dist = np.linalg.norm(self.distances)
			done = horizontal_dist < 0.08 and vertical_dist < 0.03
			#done = total_dist < 0.2
			reward = 20*done - total_dist - np.linalg.norm(action)
			if done:
				print("success!!!")
		return observation, reward, done, {"act": action}

	def find_shortest_path(self, start, goals,conf_size):
		min_vector = np.ones((conf_size,))*np.pi
		for i in range(0, int(len(goals)/conf_size), conf_size):
			goal = goals[i:i+conf_size]
			goal = self.shift_config(np.mod(np.asarray(goal), 2*pi))
			vector = self.shift_config(goal - start)
			if np.sum(np.abs(vector)) < np.sum(np.abs(min_vector)):
				min_vector = vector
		return min_vector

	def get_time(self):
		while(True):
			res, _, sim_time, _, _ = vrep.simxCallScriptFunction(self.clientID, 'Jaco', vrep.sim_scripttype_childscript, 'getTime', [], [], [], bytearray(), vrep.simx_opmode_blocking)
			if res == 0:
				return sim_time[0]
			print(res)
			time.sleep(0.1)

	def check_positions(self, joint_tol=1e-2, target_tol=1e-1):
		"""
		Check that the joints have all arrived at the target position and the grasping target
		hasn't been knocked over. Update the model if no fault is found.
		-----------------
		Input:
		joint_tol- the maximum distance a joint can be from its target position
		target_tol- the maximum distance the target can be moved
		-----------------
		Output:
		Returns True if any joint is too far away from its target position, or if the grasping
		target has moved too far.
		Returns False otherwise
		"""
		#true_pos = vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1]
		#true_pos = np.array(true_pos)
		#if np.linalg.norm(true_pos - self.target_pos) >= target_tol:
			#return 2
		#self.target_pos = true_pos
		retval = 0
		true_pos = vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2]
		for i in range(N_JOINTS):
			if np.abs(self.joint_pos[i] - true_pos[i]) >= joint_tol:
				retval = 1
				print("joints %d expected %f but was %f" %(i, self.joint_pos[i], true_pos[i]))
			self.joint_pos[i] = true_pos[i]
		angles = vrep.simxGetObjectOrientation(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1]
		for angle in angles[:-1]:
			if np.abs(angle) > pi/4:
				print("cylinder tipped over %f" %(angle*180/pi))
				retval = 1
		return retval

	def observe(self):
		_, _, obs_rgb = vrep.simxGetVisionSensorImage(self.clientID,self.rgb_sensor,0,vrep.simx_opmode_streaming)
		_, _, obs_depth = vrep.simxGetVisionSensorDepthBuffer(self.clientID,self.depth_sensor,vrep.simx_opmode_streaming)
		obs_rgb = np.asarray(obs_rgb).reshape(RESOLUTION, RESOLUTION, 3).astype(np.uint8)/255.
		obs_depth = np.asarray(obs_depth).reshape(RESOLUTION, RESOLUTION, 1)
		obs_depth /=  obs_depth.max()
		#concatenate rgb and depth readings
		observation = np.concatenate([obs_rgb, obs_depth], axis=2)
		observation = np.reshape(observation, (observation.size,))
		#concatenate joints positions
		positions  = np.divide( np.mod(self.joint_pos, 2*pi), 2*pi )
		observation = np.concatenate([observation, positions]).astype(np.float32)
		self.last_o = observation
		return observation

	def get_distances(self):
		cup_pos = vrep.simxGetObjectPosition(self.clientID, self.target, self.tip, vrep.simx_opmode_blocking)[-1]
		return np.asarray(cup_pos)

	def get_reward(self, action, expert_action, distance, done):
		if (expert_action == -1).all():
			loss = -np.linalg.norm(action) - distance
		else:
			loss = - np.linalg.norm(expert_action - action)
		return loss + done * 20

	def reset(self):
		self.goal_idx += 1
		vrep.simxSynchronous(self.clientID, False)
		vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxSynchronous(self.clientID, True)
		self.move_target()
		self.joint_pos = np.array([0, pi, pi, pi, pi, pi])
		self.move_arm()
		#self.sim_time = self.get_time()
		observation = self.observe()
		return observation

	def move_target(self):
		cur_position = self.positions[self.goal_idx%self.positions.shape[0]]
		pos_tuple = (cur_position[0], cur_position[1], cur_position[2])
		vrep.simxSetObjectPosition(self.clientID, self.target, -1, pos_tuple, vrep.simx_opmode_blocking)
		self.target_pos = cur_position

	def move_arm(self):
		noise = np.random.uniform(high=pi/8, size=(6,))
		self.joint_pos =  self.joint_pos + noise
		for i in range(N_JOINTS):
			vrep.simxSetJointPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_blocking)


	def flatten(self, array):
		return np.reshape(array, (array.size,))

	def get_vector(self):
		ik_successful, vector = self.try_ik_solution()
		if ik_successful == False:
			#mp goal
			_, vector = find_nearest_goal(self.joint_pos, self.goals[self.goal_idx%self.goals.shape[0]])
		else:
			print("IK")
		factor = 1
		maxsize =  distance_metric(vector)
		if maxsize > 0.19*pi:
			factor = 0.19*pi / maxsize
		vector *= factor
		return ik_successful, vector

	def try_ik_solution(self):
		if self.ready_for_ik() == False:
			return False, None
		inInts = [GET_IK]
		inFloats = []
		inStrings = []
		inBuffer = bytearray()
		vrep.simxSetObjectOrientation(self.clientID, self.iktarget, self.tip, (0,0,0), vrep.simx_opmode_oneshot)
		errCode, ints, floats, strings, buffer = vrep.simxCallScriptFunction(
			self.clientID, "Jaco", vrep.sim_scripttype_childscript, "approachTarget", inInts, inFloats, inStrings, inBuffer, vrep.simx_opmode_blocking)
		if errCode != 0:
			return False, None
		goal = shift_config(np.array(floats))
		vector = shift_config(goal - self.joint_pos)
		return True, vector


	def ready_for_ik(self):
		#must be positive in z axis
		if self.distances[-1] <= 0:
			return False
		#overall distance must be <=  0.1
		total_dist = np.linalg.norm(self.distances)
		print(total_dist)
		if total_dist > 0.2:
			return False
		print("near")
		#target must be within pi/5 degrees of z axis when projected onto the y axis
		y,z = self.distances[1], self.distances[2]
		angle = np.arccos(z / np.sqrt(z**2 + y**2))
		print("angle")
		if np.abs(angle) > pi/4 or (
			total_dist > 0.1 and np.abs(angle) > pi/8):
			return False
		#fingers must not intersect the target cylinder when the tip dummy is placed on the target dummy
		tip_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.tip, -1, vrep.simx_opmode_blocking)[-1])
		finger_pos = np.zeros((12,3))
		for i in range(12):
			finger_pos[i] = np.array(vrep.simxGetObjectPosition(self.clientID, self.fingers[i], -1, vrep.simx_opmode_blocking)[-1])
		#translate the vertices of the fingers to a solved position
		#check if edges are inside the target cylinder
		if self.test_intersection(tip_pos, finger_pos):
			return False
		return True

	def test_intersection(self, center_pos, finger_pos):
		print("TEST")
		#check if distance from center of cylinder each to edge is less than radius
		#center of cylinder aligns with z-axis so we project onto xy-plane
		#to get a simple 2d problem
		center = np.array(center_pos)
		# height of cylinder top and bottom when translated to the tip dummy
		ctop = center[-1] + CYLINDER_H/2
		cbot = center[-1] - CYLINDER_H/2
		center = center[:-1]
		#get indices of neighouring vertex pairs. 3 fingers, 4 vertices p. finger
		vertex_pairs = []
		for j in range(3):
			for i in range(4):
				vertex_pairs.append((i+4*j, (i+1)%4+4*j))
		
		for v1,v2 in vertex_pairs:
			p1 = finger_pos[v1]
			p2 = finger_pos[v2]
			pp1, pp2 = p1, p2
			#if segment ends above cylinder
			if p1[-1] > ctop or p2[-1] > ctop:
				t = (ctop - p1) / (p2 - p1)
				pp1 = p1 + t*(p2 - p1)
			#if segment ends below cylinder
			if p1[-1] < cbot or p2[-1] < cbot:
				t = (cbot - p1) / (p2 - p1)
				pp2 = p1 + t*(p2 - p1)
			#project to xy plane
			p1, p2 = pp1[:-1], pp2[:-1]
			edge = p2 - p1
			t = max(0, min(1, np.dot(center - p1, edge) / np.linalg.norm(edge)**2))
			projection = p1 + t * edge
			d = np.linalg.norm(projection - center)
			if d < CYLINDER_RADIUS:
				return True

	@cached_property
	def action_space(self):
		bound = np.ones(N_JOINTS) * pi / 5
		return spaces.Box(-bound, bound)

	@cached_property
	def observation_space(self):
	    return spaces.Box(low=0, high=1, shape=(RESOLUTION * RESOLUTION * N_CHANNELS + N_JOINTS,))
	
	@cached_property
	def joint_space(self):
	    lbound = np.array([-10000, 42, 20, -10000, -10000, -10000])*pi/180
	    ubound = np.array([10000, 318, 340, 10000, 10000, 10000])*pi/180
	    return spaces.Box(lbound, ubound)

