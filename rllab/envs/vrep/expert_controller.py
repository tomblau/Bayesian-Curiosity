import numpy as np
from math import pi

N_JOINTS = 6
UNBOUND_JOINTS = [0, 3, 4, 5]


def shift_config(config):
	"""shift the config so that the values of all unbounded joints
		are in the range [-pi,pi]"""
	res = config
	for idx in UNBOUND_JOINTS:
		res[idx] = np.mod(res[idx], 2*pi)
		if res[idx] > pi:
			res[idx] = res[idx] - 2*pi
		if np.abs(res[idx]) > pi + 0.1:
			sys.exit("failed to shift vector element at index %d" %idx)
	return res

def find_nearest_goal(start, goal_set):
	min_goal = np.ones((N_JOINTS,))
	min_vector = np.ones((N_JOINTS,))*pi
	for goal in goal_set:
		goal = shift_config(np.mod(goal, 2*pi))
		vector = shift_config(goal - start)
		if distance_metric(vector) < distance_metric(min_vector):
			min_goal = goal
			min_vector = vector
	return min_goal, min_vector

def distance_metric(vector):
	"""
		The distance between two configs is the sum of angle differences
		excluding the final joint because it can take arbitrary values
	"""
	return np.sum(np.abs(vector)[:-1])

class ExpertController(object):
	def __init__(self, goal_filepath, cup_filepath):
		self.goals = np.load(goal_filepath, encoding='bytes')['arr_0']
		self.positions = np.load(cup_filepath, encoding='bytes')['arr_0']
		assert self.goals.shape[0] == self.positions.shape[0]
		self.goal_idx = np.random.randint(self.goals.shape[0])

	def get_vector(self, joint_pos):
		goal, vector = find_nearest_goal(joint_pos, self.goals[self.goal_idx])
		factor = 1
		maxsize =  np.max(np.abs(vector[:-1]))
		if maxsize > 0.2*pi:
			factor = 0.2*pi / maxsize
		vector *= factor
		return vector

	def change_goal(self):
		self.goal_idx = np.random.randint(self.goals.shape[0])

	def get_pos(self):
		return self.positions[self.goal_idx]