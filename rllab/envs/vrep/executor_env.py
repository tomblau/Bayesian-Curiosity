from rllab import spaces
from rllab.envs.base import PropertyEnv
from cached_property import cached_property
import vrep
from math import pi
import time
import sys
import numpy as np
import joblib

PRETRAIN_LENGTH = 0
PRETRAIN_START = 0
PRETRAIN_END = PRETRAIN_START + PRETRAIN_LENGTH
N_JOINTS = 6
RESOLUTION = 128
N_CHANNELS = 4
XBOUNDS = (-0.565, -0.165)
YBOUNDS = (-0.2, 0.19)
Z = 0.262
JOINT_POSITION_CODE = 15
DEFAULT_JOINT_POS = [0, pi, pi, pi, pi, pi]
OMPL_PREPARE = 0
GET_GOAL = 2
TIMEOUT_CODE = 2
UNBOUND_JOINTS = [0, 3, 4, 5]
ERROR_ACTION = np.array([-1])


def shift_config(config):
	"""shift the config so that the values of all unbounded joints
		are in the range [-pi,pi]"""
	res = config
	for idx in UNBOUND_JOINTS:
		res[idx] = np.mod(res[idx], 2*pi)
		if res[idx] > pi:
			res[idx] = res[idx] - 2*pi
		if np.abs(res[idx]) > pi + 0.1:
			sys.exit("failed to shift vector element at index %d" %idx)
	return res

def find_nearest_goal(start, goal_set):
	min_goal = np.ones((N_JOINTS,))
	min_vector = np.ones((N_JOINTS,))*pi
	for goal in goal_set:
		goal = shift_config(np.mod(goal, 2*pi))
		vector = shift_config(goal - start)
		if distance_metric(vector) < distance_metric(min_vector):
			min_goal = goal
			min_vector = vector
	return min_goal, min_vector

def distance_metric(vector):
	"""
		The distance between two configs is the sum of angle differences
		excluding the final joint because it can take arbitrary values
	"""
	return np.max(np.abs(vector)[:-1])

class JacoEnv(PropertyEnv):
	def __init__(self, goal_filepath, cup_filepath):
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print("connected to sim")
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		res = vrep.simxSynchronous(self.clientID,True)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")

		res, self.rgb_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_rgb",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorImage(self.clientID, self.rgb_sensor, 0, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire rgb sensor")
		res, self.depth_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_depth",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorDepthBuffer(self.clientID, self.depth_sensor, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire depth sensor")

		res, self.tip = vrep.simxGetObjectHandle(self.clientID, "Jaco_tip0", vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire Jaco tip")
		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cup",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		self.target_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1])
		res, self.dummy = vrep.simxGetObjectHandle(self.clientID,"jacoTarget1",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target dummy")

		self.joints = [0 for _ in range(7)]
		self.joint_pos = np.zeros((N_JOINTS,))
		for i in range(1,N_JOINTS + 1):
			res, self.joints[i - 1] = vrep.simxGetObjectHandle(self.clientID, "Jaco_joint" + str(i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire joint %d" %i)
			res, self.joint_pos[i - 1] = vrep.simxGetJointPosition(self.clientID, self.joints[i - 1], vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire position for joint %d, error code: %d" %(i, res))

		res, self.joint_col = vrep.simxGetCollectionHandle(self.clientID, 'Joints', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire joint collection")
		vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)
		
		self.sim_time = 0

		self.iter = 0
		self.pretrain_iter = 0
		self.iter_mod = 0

		self.goals = np.load(goal_filepath, encoding='bytes')['arr_0']
		self.positions = np.load(cup_filepath, encoding='bytes')['arr_0']
		assert self.goals.shape[0] == self.positions.shape[0]
		self.goal_idx = 0
		self.last_o = None

		#self.demo_actions = np.load("/rllab/rllab/envs/vrep/demo_actions.npz")['arr_0']
		#self.demo_joints = np.load("/rllab/rllab/envs/vrep/demo_joints.npz")['arr_0']
		#self.demo_obs = np.load("/rllab/rllab/envs/vrep/demo_images.npz")['arr_0']
		#self.demo_dists = np.load("/rllab/rllab/envs/vrep/demo_dists.npz")['arr_0']

	def step(self, action, agent_info=None):
		#try to get an expert action
		#expert_action = self.get_expert_action()
		expert_action = np.array([-1])

		vector = self.get_vector()
		action += vector
		#action = self.action_space.sample() 
		for i in range(N_JOINTS):
			#compute new joint pos after action
			self.joint_pos[i] += action[i]
			if self.joint_pos[i] < self.joint_space.low[i]:
				self.joint_pos[i] = self.joint_space.low[i]
			elif self.joint_pos[i] > self.joint_space.high[i]:
				self.joint_pos[i] = self.joint_space.high[i]
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		#simulate 0.5 seconds of movement. Should be enough to move pi/2 radians
		old_time = self.sim_time
		vrep.simxSynchronousTrigger(self.clientID)
		vrep.simxSynchronousTrigger(self.clientID)
		self.sim_time = self.get_time()
		#make sure the simulation actually updated
		while(old_time == self.sim_time):
			print("sleeping")
			vrep.simxSynchronousTrigger(self.clientID)
			time.sleep(0.01)
			self.sim_time = self.get_time()

		stuck = self.check_positions()
		observation = self.observe()
		if stuck > 0:
			reward = -100
			done = True
			if stuck == 2:
				done = True
			if done:
				print("failure!!!")
		else:
			distances = self.get_distances()
			vertical_dist = np.linalg.norm(distances[-1])
			horizontal_dist = np.linalg.norm(distances[:-1])
			total_dist = np.linalg.norm(distances)
			done = horizontal_dist < 0.08 and vertical_dist < 0.035
			#done = total_dist < 0.2
			reward = 20*done - total_dist - np.linalg.norm(action)
			if done:
				print("success!!!")
		self.iter += 1
		return observation, reward, done, {'action' : vector, 'cup_pos': self.target_pos}

	def get_expert_action(self):
		orient = vrep.simxGetObjectOrientation(self.clientID, self.tip, -1, vrep.simx_opmode_blocking)[-1]
		vrep.simxSetObjectOrientation(self.clientID, self.dummy, -1, orient, vrep.simx_opmode_blocking)
		inInts = [OMPL_PREPARE]
		inFloats = []
		inStrings = []
		inBuffer = bytearray()
		errCode, ints, floats, strings, buffer = vrep.simxCallScriptFunction(self.clientID, "Jaco", vrep.sim_scripttype_childscript, "approachTarget", inInts, inFloats, inStrings, inBuffer, vrep.simx_opmode_blocking)
		if errCode >= 8:
			print("could not get expert action error 1")
			return ERROR_ACTION
		inInts = [GET_GOAL]
		errCode = TIMEOUT_CODE
		while errCode & TIMEOUT_CODE:
			errCode, ints, floats, strings, buffer = vrep.simxCallScriptFunction(self.clientID, "Jaco", vrep.sim_scripttype_childscript, "approachTarget", inInts, inFloats, inStrings, inBuffer, vrep.simx_opmode_blocking)
		if len(ints) == 0 or ints[0] == 0 or ints[0]%6 != 0:
			print("could not get expert action error 2")
			orient = vrep.simxGetObjectOrientation(self.clientID, self.dummy, -1, vrep.simx_opmode_blocking)[-1]
			print(self.target_pos)
			print(orient)
			return ERROR_ACTION
		start = self.shift_config(np.mod(self.joint_pos, 2*pi))
		vector = self.find_shortest_path(start, floats, 6)
		vector /= np.abs(vector).max()
		vector *= 0.2*pi
		return vector

	def shift_config(self, config):
		"""shift the config so that the values of all unbounded joints
			are in the range [-pi,pi]"""
		res = config
		for idx in UNBOUND_JOINTS:
			res[idx] = np.mod(res[idx], 2*pi)
			if res[idx] > np.pi:
				res[idx] = res[idx] - 2*pi
		return res
	
	def find_shortest_path(self, start, goals,conf_size):
		min_vector = np.ones((conf_size,))*np.pi
		for i in range(0, int(len(goals)/conf_size), conf_size):
			goal = goals[i:i+conf_size]
			goal = self.shift_config(np.mod(np.asarray(goal), 2*pi))
			vector = self.shift_config(goal - start)
			if np.sum(np.abs(vector)) < np.sum(np.abs(min_vector)):
				min_vector = vector
		return min_vector

	def get_time(self):
		while(True):
			res, _, sim_time, _, _ = vrep.simxCallScriptFunction(self.clientID, 'Jaco', vrep.sim_scripttype_childscript, 'getTime', [], [], [], bytearray(), vrep.simx_opmode_blocking)
			if res == 0:
				return sim_time[0]
			print(res)
			time.sleep(0.1)

	def check_positions(self, joint_tol=1e-2, target_tol=1e-1):
		"""
		Check that the joints have all arrived at the target position and the grasping target
		hasn't been knocked over. Update the model if no fault is found.
		-----------------
		Input:
		joint_tol- the maximum distance a joint can be from its target position
		target_tol- the maximum distance the target can be moved
		-----------------
		Output:
		Returns True if any joint is too far away from its target position, or if the grasping
		target has moved too far.
		Returns False otherwise
		"""
		#true_pos = vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1]
		#true_pos = np.array(true_pos)
		#if np.linalg.norm(true_pos - self.target_pos) >= target_tol:
			#return 2
		#self.target_pos = true_pos
		retval = 0
		true_pos = vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2]
		for i in range(N_JOINTS):
			if np.abs(self.joint_pos[i] - true_pos[i]) >= joint_tol:
				retval = 1
				print("joints %d expected %f but was %f" %(i, self.joint_pos[i], true_pos[i]))
			self.joint_pos[i] = true_pos[i]
		return retval

	def observe(self):
		_, _, obs_rgb = vrep.simxGetVisionSensorImage(self.clientID,self.rgb_sensor,0,vrep.simx_opmode_streaming)
		_, _, obs_depth = vrep.simxGetVisionSensorDepthBuffer(self.clientID,self.depth_sensor,vrep.simx_opmode_streaming)
		obs_rgb = np.asarray(obs_rgb).reshape(RESOLUTION, RESOLUTION, 3).astype(np.uint8)/255.
		obs_depth = np.asarray(obs_depth).reshape(RESOLUTION, RESOLUTION, 1)
		obs_depth /=  obs_depth.max()
		#concatenate rgb and depth readings
		observation = np.concatenate([obs_rgb, obs_depth], axis=2)
		observation = np.reshape(observation, (observation.size,))
		#concatenate joints positions
		positions  = np.divide( np.mod(self.joint_pos, 2*pi), 2*pi )
		observation = np.concatenate([observation, positions]).astype(np.float32)
		self.last_o = observation
		return observation

	def get_distances(self):
		cup_pos = vrep.simxGetObjectPosition(self.clientID, self.target, self.tip, vrep.simx_opmode_blocking)[-1]
		return np.asarray(cup_pos)

	def get_reward(self, action, expert_action, distance, done):
		if (expert_action == -1).all():
			loss = -np.linalg.norm(action) - distance
		else:
			loss = - np.linalg.norm(expert_action - action)
		return loss + done * 20

	def reset(self):
		self.goal_idx += 1
		vrep.simxSynchronous(self.clientID, False)
		vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxSynchronous(self.clientID, True)
		self.move_target()
		#self.move_arm()
		#self.sim_time = self.get_time()
		#self.joint_pos = np.array(vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2])
		self.joint_pos = np.array([0, pi, pi, pi, pi, pi])
		observation = self.observe()
		return observation

	def move_target(self):
		cur_position = self.positions[self.goal_idx%self.positions.shape[0]]
		pos_tuple = (cur_position[0], cur_position[1], cur_position[2])
		vrep.simxSetObjectPosition(self.clientID, self.target, -1, pos_tuple, vrep.simx_opmode_blocking)
		self.target_pos = cur_position
		print(cur_position)

	def move_arm(self):
		lower_bound = np.array([0, 160, 90, 0, 0, 0])*pi/180
		upper_bound = np.array([360, 200, 270, 360, 360, 360])*pi/180
		self.joint_pos = np.random.uniform(lower_bound, upper_bound)
		for i in range(N_JOINTS):
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		for _ in range(10):
			vrep.simxSynchronousTrigger(self.clientID)

	def flatten(self, array):
		return np.reshape(array, (array.size,))

	def get_vector(self):
		goal, vector = find_nearest_goal(self.joint_pos, self.goals[self.goal_idx%self.goals.shape[0]])
		factor = 1
		maxsize =  np.max(np.abs(vector[:-1]))
		if maxsize > 0.19*pi:
			factor = 0.19*pi / maxsize
		vector *= factor
		return vector

	@cached_property
	def action_space(self):
		bound = np.ones(N_JOINTS) * pi / 5
		return spaces.Box(-bound, bound)

	@cached_property
	def observation_space(self):
	    return spaces.Box(low=0, high=1, shape=(RESOLUTION * RESOLUTION * N_CHANNELS + N_JOINTS,))
	
	@cached_property
	def joint_space(self):
	    lbound = np.array([-10000, 42, 20, -10000, -10000, -10000])*pi/180
	    ubound = np.array([10000, 318, 340, 10000, 10000, 10000])*pi/180
	    return spaces.Box(lbound, ubound)

