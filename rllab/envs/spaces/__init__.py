from rllab.envs.spaces.box import Box
from rllab.envs.spaces.discrete import Discrete
from rllab.envs.spaces.multi_discrete import MultiDiscrete
from rllab.envs.spaces.multi_binary import MultiBinary
from rllab.envs.spaces.prng import seed, np_random
from rllab.envs.spaces.tuple_space import Tuple
from rllab.envs.spaces.dict_space import Dict

__all__ = ["Box", "Discrete", "MultiDiscrete", "MultiBinary", "Tuple", "Dict"]
