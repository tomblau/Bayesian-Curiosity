import numpy as np
from numpy import pi
from rllab import spaces
import gym
import math
from rllab.envs.base import PropertyEnv
from cached_property import cached_property


class MountainCarEnv(PropertyEnv):
	def __init__(self, seed=None, human=False):
		"""
		history_len - the length of observation history to maintain
		frame_skip - the distance between subsequent frames recorded in the history. By default record
			every 2nd frame
		human - enable human interaction
		"""
		self.env = gym.envs.make("MountainCarContinuous-v0")
		self.env.env.action_space = spaces.Box(low=np.array([-1.]), high=np.array([1.]), dtype=np.float32)
		self.human = human
		if self.human:
			self.viewer = self.env.env.viewer
		self.env.env.seed(seed)

	def reset(self):
		return self.env.env.reset()

	def step(self, action):
		position = self.env.env.state[0]
		velocity = self.env.env.state[1]
		force = min(max(action[0], -1.0), 1.0)

		velocity += force*self.env.env.power -0.0025 * math.cos(3*position)
		if (velocity > self.env.env.max_speed): velocity = self.env.env.max_speed
		if (velocity < -self.env.env.max_speed): velocity = -self.env.env.max_speed
		position += velocity
		if (position > self.env.env.max_position): position = self.env.env.max_position
		if (position < self.env.env.min_position): position = self.env.env.min_position
		if (position==self.env.env.min_position and velocity<0): velocity = 0

		done = bool(position >= self.env.env.goal_position)

		reward = -1.
		if done:
			reward = 100.0

		self.env.env.state = np.array([position, velocity])
		obs = self.env.env.state
		return self.env.env.state, reward, done, {'success' : done, 'raw_obs' : obs}


	def render(self):
		self.env.env.render()

	def close(self):
		self.env.env.close()

	@cached_property
	def action_space(self):
		return self.env.env.action_space

	@cached_property
	def observation_space(self):
		return spaces.Box(low=self.env.env.observation_space.low,
						  high=self.env.env.observation_space.high,
						  dtype=self.env.env.observation_space.low.dtype)

	@property
	def horizon(self):
		return 200
