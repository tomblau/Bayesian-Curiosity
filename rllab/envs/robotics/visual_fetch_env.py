from rllab.envs.robotics.fetch_env import FetchEnv
import numpy as np


AZIMUTH = 132.0
DISTANCE = 1.25
ELEVATION = -14.0
LOOKAT = [1.2, 0.75, 0.625]
RESOLUTION = 256


class VisualFetchEnv(FetchEnv):
    def __init__(
        self, model_path, n_substeps, gripper_extra_height, block_gripper,
        has_object, target_in_the_air, target_offset, obj_range, target_range,
        distance_threshold, initial_qpos, reward_type,
    ):
        super(VisualFetchEnv, self).__init__(
            model_path, n_substeps, gripper_extra_height, block_gripper,
            has_object, target_in_the_air, target_offset, obj_range, target_range,
            distance_threshold, initial_qpos, reward_type,
        )
        #self.sim.render(RESOLUTION,RESOLUTION)
        #self.ctx = self.sim._render_context_offscreen
        self._render_callback()
        self.offcam = self.ctx.cam
        self.offcam.azimuth = AZIMUTH
        self.offcam.distance = DISTANCE
        self.offcam.elevation = ELEVATION

    def reset(self):
        res = super(VisualFetchEnv, self).reset()
        for i in range(3):
            self.offcam.lookat[i] = LOOKAT[i]
        self.step(np.zeros(4))
        self._render_callback()
        return self._get_obs()

    def _get_obs(self):
        obs = super(VisualFetchEnv, self)._get_obs()
        self.ctx.render(RESOLUTION,RESOLUTION)
        img = list(self.ctx.read_pixels(RESOLUTION,RESOLUTION,depth=True))
        img[0] = img[0]/255.
        img[1].shape = (RESOLUTION,RESOLUTION,1)
        rgbd = np.concatenate(img, axis=2)[::-1,:,:]
        rgbd = np.reshape(rgbd, (rgbd.size,)).astype(np.float32)
        obs['observation'] = np.concatenate([rgbd, obs['observation']])
        return obs