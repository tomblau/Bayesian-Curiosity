import theano.tensor as TT
from theano import shared, function
import numpy as np
import ghalton as gh
import scipy.stats as sstats


class BayesianLinearRegression(object):
	def __init__(
		self,
		feature_dim,
		output_dim,
		init_m0=0.,
		init_alpha=1e-2,
		init_beta=1e2
	):
		#add one dimension for bias
		self.feature_dim = feature_dim + 1
		self.output_dim = output_dim
		self.def_alpha = float(init_alpha)
		self.def_beta = float(init_beta)
		self.def_m0 = float(init_m0)
		self.alpha = shared( self.def_alpha * np.ones((output_dim,)) )
		self.beta = shared( self.def_beta * np.ones((output_dim,)) )
		self.m0 = shared( self.def_m0 * np.ones((self.feature_dim, output_dim)) )
		idmat = np.stack([np.eye(self.feature_dim) for _ in range(output_dim)])
		self.S0 = shared( 1/float(init_alpha) * idmat )
		self.S0_inv = shared( float(init_alpha) * idmat )
		self.mN = self.m0
		self.SN = self.S0
		self.opts = {}

	def get_params(self):
		return [self.alpha, self.beta, self.m0, self.S0, self.S0_inv]

	def get_moment_vars(self, phi):
		phi = self.add_bias(phi)
		mean_var = phi.dot( self.mN )
		std_var = 1 / self.beta + \
			TT.batched_dot(
				TT.tensordot(phi, self.SN, [[1], [1]]),
				phi.dimshuffle([0,1,'x'])
			).dimshuffle([0,1])
		log_std_var = TT.log(std_var)
		return mean_var, std_var, log_std_var

	def reset_all_values(self):
		self.reset_hyperparams()
		self.reset_params()

	def reset_params(self):
		self.m0.set_value( self.def_m0 * np.ones((self.feature_dim, self.output_dim)) )
		self.S0.set_value(
			TT.tensordot(1 / self.alpha, TT.eye(self.feature_dim), [[],[]]).eval()
		)
		self.S0_inv.set_value(
			TT.tensordot(self.alpha, TT.eye(self.feature_dim), [[],[]]).eval()
		)

	def reset_hyperparams(self):
		self.alpha.set_value( float(self.def_alpha) * np.ones((self.output_dim,)) )
		self.beta.set_value( float(self.def_beta) * np.ones((self.output_dim,)) )		

	def update_opt(self, obs_var, phi):
		targets = TT.matrix("targets")
		phi = self.add_bias(phi)
		p = phi.astype("float64")
		ptp = p.T.dot(p)
		updates = self._p_update_helper(ptp, p, targets)
		self.opts['f_p_update'] = function(inputs=[obs_var, targets], updates=updates)

		self._iterative_update_opt()

	def _iterative_update_opt(self):
		targets = TT.matrix("targets_iterative")
		p = TT.dmatrix("features")
		ptp = TT.dmatrix("ptp")
		updates = self._p_update_helper(ptp, p, targets)
		self.opts['f_p_update_iterative'] = function(
			inputs=[p, ptp, targets], updates=updates)

		updates = self._p_update_helper(ptp)
		self.opts['f_p_update_s_iterative'] = function(
			inputs=[ptp], updates=updates)

		lamd = TT.dmatrix("lambda")
		updates = self._hp_update_helper(p, lamd, targets)
		self.opts['f_hp_update_iterative'] = function(
			inputs=[p, lamd, targets], updates=updates)

		updates = self._smw_update_helper(p)
		self.opts['smw_s_update'] = function(
			inputs=[p], updates=updates)

	def _p_update_helper(self, ptp, p=None, targets=None):
		eye = TT.eye(self.feature_dim)
		#this tensordot expands ptp to a vector of matrices where the i-th
		#entry is beta[i]*ptp
		new_SN_inv = self.S0_inv + TT.tensordot(self.beta, ptp, [[], []])
		#because SN is symmetric, we can use SN^-1 = QDQ^T which is more stable
		#tensor.matrix_inverse sometimes gives weird results
		L = [TT.slinalg.cholesky(new_SN_inv[i]) for i in range(self.output_dim)]
		y = [TT.slinalg.solve_lower_triangular(L[i], eye) for i in range(self.output_dim)]
		x = [TT.slinalg.solve_upper_triangular(L[i].T, y[i]) for i in range(self.output_dim)]
		new_SN = TT.stack(x)
		#eigres = [TT.nlinalg.eigh(new_SN_inv[i]) for i in range(self.output_dim)]
		#Q = TT.stack([eigres[i][1] for i in range(self.output_dim)])
		#D = TT.stack([ TT.nlinalg.diag( TT.inv(eigres[i][0]) )for i in range(self.output_dim)])
		#new_SN = TT.batched_dot( TT.batched_dot(Q, D), Q.dimshuffle([0,2,1]) )
		if targets is not None and p is not None:
			new_mN = TT.batched_dot(
				new_SN,
				(TT.batched_dot(
					self.S0_inv,
					self.m0.dimshuffle([1,0,'x'])
				) + ( self.beta.dimshuffle([0,'x']) * targets.T.dot(p) ).dimshuffle([0,1,'x'])
				)
			).dimshuffle([1,0])
			updates = [
				(self.m0, TT.unbroadcast(new_mN, 1)),
				(self.S0, TT.unbroadcast(new_SN, 0)),
				(self.S0_inv, TT.unbroadcast(new_SN_inv, 0)),
			]
		else:
			updates = [
				(self.S0, TT.unbroadcast(new_SN, 0)),
				(self.S0_inv, TT.unbroadcast(new_SN_inv, 0)),
			]
		return updates

	def _hp_update_helper(self, p, lamd, targets):
		gamma = TT.sum(lamd / (lamd + self.alpha.dimshuffle([0,'x'])), axis=1)
		new_alpha = gamma / (TT.sum(self.mN**2, axis=0) + 1e-10)
		nmgamma = p.shape[0] - gamma
		sqerror = TT.sum( TT.square(targets - p.dot(self.mN)), axis=0 )
		new_beta = nmgamma * TT.inv(sqerror)
		updates = [
			(self.alpha, new_alpha),
			(self.beta, new_beta)
		]
		return updates

	def _smw_update_helper(self, p):
		p = self.add_bias(p)
		ptp = p.T.dot(p)
		ptsp = TT.batched_dot(
				TT.tensordot(p, self.SN, [[1], [1]]),
				p.dimshuffle([0,1,'x'])
			).dimshuffle([0,1])
		numer = TT.batched_dot(
					TT.tensordot(self.SN, ptp, [[1], [1]]),
					self.SN
				)
		inv_denom = (self.beta/(1 + self.beta * ptsp)).dimshuffle([1,0,'x'])
		inv_denom = TT.addbroadcast(inv_denom, 1)
		ratio = inv_denom * numer
		new_SN = self.SN - ratio
		updates = [(self.SN, new_SN)]
		return updates

	#learn the "hyperparameters" alpha and beta based on data
	def learn_all_params(self, phi, targets):
		phi = self.add_bias(phi)
		p = phi.astype("float64")
		ptp = p.T.dot(p)
		prelamd, _ = TT.nlinalg.eigh(ptp)
		update_param = self.opts['f_p_update_iterative']
		update_hyperparam = self.opts['f_hp_update_iterative']
		for _ in range(10):
			lamd = TT.outer(self.beta, prelamd).eval()
			self.reset_params()
			update_param(p, ptp, targets)
			update_hyperparam(p, lamd, targets)
		
	def dist_info_sym(self, phi, phip, targets, targetsp):
		#pretend to have an untrained BLR
		eye = TT.eye(self.feature_dim)
		old_m0 = self.def_m0 * TT.ones((self.feature_dim, self.output_dim))
		old_S0 = TT.tensordot(1 / self.alpha, eye, [[],[]]).eval()
		old_S0_inv = TT.tensordot(self.alpha, eye, [[],[]]).eval()
		phi = self.add_bias(phi)
		p = phi.astype("float64")
		ptp = p.T.dot(p)

		#pretend to train BLR only on (phi, targets)
		new_S0_inv = old_S0_inv + TT.tensordot(self.beta, ptp, [[], []])
		L = [TT.slinalg.cholesky(new_S0_inv[i]) for i in range(self.output_dim)]
		y = [TT.slinalg.solve_lower_triangular(L[i], eye) for i in range(self.output_dim)]
		x = [TT.slinalg.solve_upper_triangular(L[i].T, y[i]) for i in range(self.output_dim)]
		new_S0 = TT.stack(x)
		eigres = [TT.nlinalg.eigh(new_S0_inv[i]) for i in range(self.output_dim)]
		Q = TT.stack([eigres[i][1] for i in range(self.output_dim)])
		D = TT.stack([TT.inv(eigres[i][0]) for i in range(self.output_dim)])
		#new_S0 = TT.batched_dot( TT.batched_dot(Q, D), Q.dimshuffle([0,2,1]) )
		new_m0 = TT.batched_dot(
			new_S0,
			(TT.batched_dot(
				old_S0,
				old_m0.dimshuffle([1,0,'x'])
			) + ( self.beta.dimshuffle([0,'x']) * targets.T.dot(p) ).dimshuffle([0,1,'x'])
			)
		).dimshuffle([1,0])
		#get posterior for phi'. These will be tensors of variables that depend on 
		#the neural network parameters
		pp = self.add_bias(phip)
		mean_var = pp.dot(new_m0)
		std_var = 1 / self.beta + \
			TT.batched_dot(
				TT.tensordot(pp, new_S0, [[1], [1]]),
				pp.dimshuffle([0,1,'x'])
			).dimshuffle([0,1])
		z = (TT.tensordot(pp, Q, [[1], [1]]) ** 2) * D
		sqerrors = TT.sum( TT.square(targetsp - mean_var), axis=0)
		l2s = TT.diag( new_m0.T.dot(new_m0) )
		dets = TT.stack( [2 * TT.log( TT.diag(L[i]) ).sum() for i in range(self.output_dim)] )
		lml =  (self.feature_dim/2) * TT.log(self.alpha) \
			+ (phip.shape[0]/2) * TT.log(self.beta) \
			- (self.beta/2) * sqerrors \
			- (self.alpha/2) * l2s \
			- 0.5 * dets \
			- (phip.shape[0]/2) * TT.log(2*np.pi)
		return dict(mean=mean_var, std=std_var, log_std=TT.log(std_var),
			lml=lml, sqerrors=sqerrors, l2s=l2s, dets=dets, z=z)

	def log_marginal_likelihood_sym(self, phi, targets):
		#pretend to have an untrained BLR
		old_m0 = self.def_m0 * TT.ones((self.feature_dim, self.output_dim))
		old_S0 = TT.tensordot(1 / self.alpha, TT.eye(self.feature_dim), [[],[]]).eval()
		old_S0_inv = TT.tensordot(self.alpha, TT.eye(self.feature_dim), [[],[]]).eval()
		phi = self.add_bias(phi)
		p = phi.astype("float64")
		ptp = p.T.dot(p)
		#pretend to train BLR on (phi, targets)
		new_S0_inv = old_S0_inv + TT.tensordot(self.beta, ptp, [[], []])
		eigres = [TT.nlinalg.eigh(new_S0_inv[i]) for i in range(self.output_dim)]
		Q = TT.stack([eigres[i][1] for i in range(self.output_dim)])
		D = TT.stack([ TT.nlinalg.diag( TT.inv(eigres[i][0]) )for i in range(self.output_dim)])
		new_S0 = TT.batched_dot( TT.batched_dot(Q, D), Q.dimshuffle([0,2,1]) )
		new_m0 = TT.batched_dot(
			new_S0,
			(TT.batched_dot(
				old_S0,
				old_m0.dimshuffle([1,0,'x'])
			) + ( self.beta.dimshuffle([0,'x']) * targets.T.dot(p) ).dimshuffle([0,1,'x'])
			)
		).dimshuffle([1,0])
		#get lml
		lml =  (self.feature_dim/2) * TT.log(self.alpha) \
			+ (phip.shape[0]/2) * TT.log(self.beta) \
			- (self.beta/2) * TT.sum( TT.square(targets - p.dot(new_m0)), axis=0) \
			- (self.alpha/2) * TT.diag( new_m0.T.dot(new_m0) ) \
			- TT.stack( [TT.sum(TT.log(eigres[i][0])) for i in range(self.output_dim)] ) \
			- (phip.shape[0]/2) * TT.log(2*np.pi)

	def add_bias(self, phi):
		#add dummy bias variable to feature vector phi
		if isinstance(phi, np.ndarray):
			return np.concatenate([phi, np.ones_like(phi[:,-1:])], axis=1)
		return TT.concatenate([phi, TT.ones_like(phi[:,-1:])], axis=1)

class Kernel(object):
	def transform(self, X):
		raise NotImplementedError

class IdentityKernel(Kernel):
	def transform(self, X):
		return X

class RFFKernel(Kernel):
	def __init__(self, init_ls=1., M=100, d=10):
		ls = np.log(np.exp(init_ls) - 1)
		self.ls = shared(ls)
		self.M = M
		self.d = d
		self.W = shared(np.ones((self.M, self.d)))
		self.sample()

	def sample(self):
		raise NotImplementedError

	def transform(self, X):
		X = X/TT.nnet.softplus(self.ls)
		XWT = X.dot(self.W.T)
		phi = TT.sqrt(1/self.M) * TT.concatenate([TT.cos(XWT), TT.sin(XWT)], axis=1)
		return phi

	def get_params(self):
		return [self.ls]


class GaussianRFFKernel(RFFKernel):
	def sample(self):
		self.W.set_value(np.random.normal(size=(self.M,self.d)))


class QMCRFFKernel(RFFKernel):
	def sample(self):
		generator = gh.GeneralizedHalton(gh.EA_PERMS[:self.d])
		sequence = np.array(generator.get(self.M))
		self.W.set_value(sstats.norm.ppf(sequence))


