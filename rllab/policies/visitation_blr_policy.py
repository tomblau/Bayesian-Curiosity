import lasagne
import lasagne.layers as L
import lasagne.nonlinearities as NL
import numpy as np

from rllab.core.serializable import Serializable
from rllab.spaces import Box

from rllab.policies.blr_policy import BLRPolicy
from rllab.misc.overrides import overrides
from rllab.misc import logger
from rllab.misc import ext
from rllab.distributions.diagonal_gaussian import DiagonalGaussian
import theano.tensor as TT
from theano import shared, function


class VisitationBLRPolicy(BLRPolicy):
    def __init__(
            self,
            env_spec,
            network,
            vis_network,
            regression,
            vis_regression,
            kernel,
            dist_cls=DiagonalGaussian,
            eta=1.0,
            **kwargs
    ):
        """
        Like a BLRPolicy but also maintains a secondary BLR with a static feature map
        that keeps track of visitation information and uses it to generate intrinsic
        rewards
        :param env_spec: a spec of state/action space dimensionality for the related environment
        :param regression: the BLR that sits on top of the network
        :param kernel: a kernel that transforms the network output before the BLR
        :param network: custom network for the output mean
        :return:
        """
        Serializable.quick_init(self, locals())
        super(VisitationBLRPolicy, self).__init__(
            env_spec, regression, kernel, network=network, **kwargs)

        self.eta = eta
        self._v_network = vis_network
        v_l_out = vis_network.output_layer
        v_obs_var = vis_network.input_layer.input_var
        self._v_l_out = v_l_out
        self._v_regression = vis_regression
        v_phi = L.get_output(self._v_l_out, deterministic=True)
        v_phi = self._kernel.transform(v_phi)
        v_mean_var, v_std_var, v_log_std_var = self._v_regression.get_moment_vars(v_phi)
        
        self._v_regression.update_opt(v_obs_var, v_phi)

        self._f_v_phis = ext.compile_function(
            inputs=[v_obs_var],
            outputs=v_phi
            )
        self._f_vis_unc = ext.compile_function(
            inputs=[v_obs_var],
            outputs=[v_log_std_var],
        )

    def get_vis_phis_sym(self, obs_var, deterministic=True):
        phi= L.get_output(self._v_l_out, obs_var, deterministic=deterministic)
        return self._kernel.transform(phi)

    def get_vis_phis(self, observations):
        flat_obs = self.observation_space.flatten_n(observations)
        phis = self._f_v_phis(flat_obs)
        return phis

    def vis_raw_update(self, o):
        #sanity check
        if len(o.shape) == 1:
            o = o.reshape(1,o.shape[0])
        phi = self.get_phis(o)
        self._v_regression.opts['smw_s_update'](phi)

    def vis_phi_update(self, phi):
        self._v_regression.opts['smw_s_update'](phi)

    def get_intrinsic_reward(self, o):
        phi = self.get_phis(o)
        unc = self._f_vis_unc(phi)
        return self.eta * unc

    def get_intrinsic_phi_reward(self, phi):
        unc = self._f_phi_unc(phi)[0][0,0]
        return self.eta * unc



    @property
    @overrides
    def intrinsic(self):
        return True