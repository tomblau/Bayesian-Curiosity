import lasagne
import lasagne.layers as L
import lasagne.nonlinearities as NL
import numpy as np

from rllab.core.lasagne_powered import LasagnePowered
from rllab.core.network import MLP
from rllab.core.serializable import Serializable
from rllab.spaces import Box

from rllab.policies.base import StochasticPolicy
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import GaussianRFFKernel as GRFFK
from rllab.misc.overrides import overrides
from rllab.misc import logger
from rllab.misc import ext
from rllab.distributions.diagonal_gaussian import DiagonalGaussian
import theano.tensor as TT
from theano import shared, function


class BLRPolicy(StochasticPolicy, LasagnePowered, Serializable):
    def __init__(
            self,
            env_spec,
            regression,
            kernel,
            hidden_sizes=(32, 32),
            feature_dim=32,
            hidden_nonlinearity=NL.tanh,
            output_nonlinearity=None,
            network=None,
            dist_cls=DiagonalGaussian,
            m0=0.,
            alpha=1e-2,
            beta=1e2,
            num_rff=25,
            **kwargs
    ):
        """
        A policy that implements Bayesian Linear Regression on top of a Neural Network
        optionally includes a kernel (e.g. Random Fourier Features) between the two
        WARNING: if regression, kernel or network are not provided as arguments,
        they will be re-created with default values when you pickle/unpickle
        :param env_spec: a spec of state/action space dimensionality for the related environment
        :param regression: the BLR that sits on top of the network
        :param kernel: a kernel that transforms the network output before the BLR
        :param hidden_sizes: list of sizes for the fully-connected hidden layers
        :param learn_std: Is std trainable
        :param adaptive_std:
        :param std_share_network:
        :param std_hidden_sizes: list of sizes for the fully-connected layers for std
        :param std_hidden_nonlinearity:
        :param hidden_nonlinearity: nonlinearity used for each hidden layer
        :param output_nonlinearity: nonlinearity for the output layer
        :param network: custom network for the output mean
        :param std_network: custom network for the output log std
        :return:
        """
        Serializable.quick_init(self, locals())
        assert isinstance(env_spec.action_space, Box)

        obs_dim = env_spec.observation_space.flat_dim
        action_dim = env_spec.action_space.flat_dim
        self._obs_dim = obs_dim
        self._action_dim = int(action_dim)

        # create network
        if network is None:
            network = MLP(
                input_shape=(obs_dim,),
                output_dim=feature_dim,
                hidden_sizes=hidden_sizes,
                hidden_nonlinearity=hidden_nonlinearity,
                output_nonlinearity=output_nonlinearity,
            )
        self._network = network

        l_out = network.output_layer
        obs_var = network.input_layer.input_var
        self._l_out = l_out

        if kernel is None:
            kernel = GRFFK(M=num_rff * feature_dim, d=feature_dim)
        self._kernel = kernel

        if regression is None:
            regression = BLR(2*self._kernel.M, action_dim, m0, alpha, beta)
        self._regression = regression


        phi = L.get_output(self._l_out, deterministic=True)
        phi = self._kernel.transform(phi)
        mean_var, std_var, log_std_var = self._regression.get_moment_vars(phi)
        self._mean_var, self._std_var, self._log_std_var = mean_var, std_var, log_std_var

        self._dist = dist_cls(action_dim)

        LasagnePowered.__init__(self, [l_out])
        super(BLRPolicy, self).__init__(env_spec)

        #prediction function
        self._f_dist = ext.compile_function(
            inputs=[obs_var],
            outputs=[mean_var, log_std_var],
        )
        self._f_phis = ext.compile_function(
            inputs=[obs_var],
            outputs=phi
            )

        #create BLR parameter update functions
        self._regression.update_opt(obs_var, phi)
        self._f_blr_update = self._regression.opts['f_p_update']

    def dist_info_sym(self, obs_var, state_info_vars=None, deterministic=True, train_clip=True):
        phi = self.get_phis_sym(obs_var)
        mean_var, std_var, log_std_var = self._regression.get_moment_vars(phi)
        return dict(mean=mean_var, std=std_var, log_std=log_std_var)

    @overrides
    def get_action(self, observation):
        flat_obs = self.observation_space.flatten(observation)
        mean, log_std = [x[0] for x in self._f_dist([flat_obs])]
        rnd = np.random.normal(size=mean.shape)
        action = rnd * np.exp(log_std) + mean
        #print(mean)
        #print(np.exp(log_std))
        return action, dict(mean=mean, log_std=log_std)
    
    def get_actions(self, observations):
        flat_obs = self.observation_space.flatten_n(observations)
        means, log_stds = self._f_dist(flat_obs)
        rnd = np.random.normal(size=means.shape)
        actions = rnd * np.exp(log_stds) + means
        return actions, dict(mean=means, log_std=log_stds)

    def get_reparam_action_sym(self, obs_var, action_var, old_dist_info_vars):
        """
        Given observations, old actions, and distribution of old actions, return a symbolically reparameterized
        representation of the actions in terms of the policy parameters
        :param obs_var:
        :param action_var:
        :param old_dist_info_vars:
        :return:
        """
        new_dist_info_vars = self.dist_info_sym(obs_var, action_var)
        new_mean_var, new_log_std_var = new_dist_info_vars["mean"], new_dist_info_vars["log_std"]
        old_mean_var, old_log_std_var = old_dist_info_vars["mean"], old_dist_info_vars["log_std"]
        epsilon_var = (action_var - old_mean_var) / (TT.exp(old_log_std_var) + 1e-8)
        new_action_var = new_mean_var + epsilon_var * TT.exp(new_log_std_var)
        return new_action_var

    def log_diagnostics(self, paths):
        log_stds = np.vstack([path["agent_infos"]["log_std"] for path in paths])
        logger.record_tabular('AveragePolicyStd', np.mean(np.exp(log_stds), axis=0))

    def reset_regression(self):
        self._regression.reset_params()

    def get_phis_sym(self, obs_var, deterministic=True):
        phi= L.get_output(self._l_out, obs_var, deterministic=deterministic)
        return self._kernel.transform(phi)


    def get_phis(self, observations):
        flat_obs = self.observation_space.flatten_n(observations)
        phis = self._f_phis(flat_obs)
        return phis

    def phi_blr_update(self, phi, targets):
        """
        update the blr using input in feature space
        """
        phi = self._regression.add_bias(phi)
        ptp = phi.T.dot(phi)
        self._regression.opts['f_p_update_iterative'](phi, ptp, targets)
    
    @property
    def distribution(self):
        return self._dist

    @property
    def update_fn(self):
        return self._f_blr_update

    @property
    def smw_update_fn(self):
        return self._regression.opts['smw_s_update']

    #@overrides
    #def get_params(self, **tags):  # adds the list to the _cached_params dict under the tuple key (one)
        #"""
        #Get the list of parameters, filtered by the provided tags.
        #Some common tags include 'regularizable' and 'trainable'
        #"""
        #params = L.get_all_params(self._l_out, **tags)
        #params.extend([self._regression.m0, self._regression.S0])
        #return params
#
    #@overrides
    #def get_param_shapes(self, **tags):
        #params = self.get_params(**tags)
        #return [p.get_value(borrow=True).shape for p in params]
#
    #@overrides
    #def get_param_dtypes(self, **tags):
        #params = self.get_params(**tags)
        #return [p.get_value(borrow=True).dtype for p in params]

    @property
    @overrides
    def bayesian(self):
        return False

    #@overrides
    #def get_params_internal(self, **tags):  # this gives ALL the vars (not the params values)
        #params = L.get_all_params(  # this lasagne function also returns all var below the passed layers
            #L.concat(self._output_layers),
            #**tags
        #)
        #params.extend(self._kernel.get_params())
        #return params