import lasagne
import lasagne.layers as L
import lasagne.nonlinearities as NL
import lasagne.init as LI

from rllab.core.lasagne_powered import LasagnePowered
from rllab.core.serializable import Serializable
from rllab.core.parameterized import Parameterized
from rllab.misc import ext
from rllab.misc.overrides import overrides
from rllab.policies.base import Policy
import theano.tensor as TT

import inspect

class VisitationDeterministicPolicy(Policy, LasagnePowered):
    def __init__(
            self,
            env_spec,
            network,
            vis_network,
            vis_regression,
            kernel,
            eta=1.0,
            ):
        Serializable.quick_init(self, locals())

        obs_dim = env_spec.observation_space.flat_dim
        action_dim = env_spec.action_space.flat_dim

        self._network = network
        l_output = network.output_layer
        self._l_out = l_output

        # Note the deterministic=True argument. It makes sure that when getting
        # actions from single observations, we do not update params in the
        # batch normalization layers

        action_var = L.get_output(l_output, deterministic=True)
        stochastic_action_var = L.get_output(l_output)
        self._output_layer = l_output

        self._f_actions = ext.compile_function([network.input_var], action_var)
        self._stochastic_f_actions = ext.compile_function([network.input_var], stochastic_action_var)

        super(VisitationDeterministicPolicy, self).__init__(env_spec)
        LasagnePowered.__init__(self, [l_output])

        self.eta = eta
        self._v_network = vis_network
        self._v_regression = vis_regression
        self._kernel = kernel

        v_l_out = vis_network.output_layer
        v_obs_var = vis_network.input_layer.input_var
        self._v_l_out = v_l_out
        v_phi = L.get_output(self._v_l_out, deterministic=True)
        v_phi = self._kernel.transform(v_phi)
        self._v_regression.update_opt(v_obs_var, v_phi)
        v_mean_var, v_std_var, v_log_std_var = self._v_regression.get_moment_vars(v_phi)

        phi_var = TT.dmatrix("phis")
        phi_mean_var, phi_std_var, phi_log_std_var = self._v_regression.get_moment_vars(phi_var)
        

        self._f_v_phis = ext.compile_function(
            inputs=[v_obs_var],
            outputs=v_phi
            )
        self._f_vis_unc = ext.compile_function(
            inputs=[v_obs_var],
            outputs=[v_log_std_var],
        )
        self._f_phi_unc = ext.compile_function(
            inputs=[phi_var],
            outputs=[phi_log_std_var],
        )


    def get_action(self, observation):
        action = self._f_actions([observation])[0]
        return action, dict()

    def get_stochastic_action(self, observation):
        action = self._stochastic_f_actions([observation])[0]
        return action, dict()

    def get_actions(self, observations):
        return self._f_actions(observations), dict()

    def get_action_sym(self, obs_var):
        return L.get_output(self._output_layer, obs_var, deterministic=True)

    def get_vis_phis_sym(self, obs_var, deterministic=True):
        phi= L.get_output(self._v_l_out, obs_var, deterministic=deterministic)
        return self._kernel.transform(phi)

    def get_vis_phis(self, observations):
        flat_obs = self.observation_space.flatten_n(observations)
        phis = self._f_v_phis(flat_obs)
        return phis

    def vis_raw_update(self, o):
        #sanity check
        if len(o.shape) == 1:
            o = o.reshape(1,o.shape[0])
        phi = self.get_vis_phis(o)
        self._v_regression.opts['smw_s_update'](phi)

    def vis_phi_update(self, phi):
        self._v_regression.opts['smw_s_update'](phi)

    def vis_phi_batch_update(self, phi):
        """
        update the blr using input in feature space
        """
        phi = self._v_regression.add_bias(phi)
        ptp = phi.T.dot(phi)
        self._v_regression.opts['f_p_update_s_iterative'](ptp)

    def get_intrinsic_reward(self, o):
        phi = self.get_vis_phis(o)
        unc = self._f_vis_unc(phi)
        return self.eta * unc + 1.5

    def get_intrinsic_phi_reward(self, phi):
        unc = self._f_phi_unc(phi)[0][:,0]
        return self.eta * unc + 1.5



    @property
    @overrides
    def intrinsic(self):
        return True

    def __getstate__(self):
        d = Parameterized.__getstate__(self)
        spec = inspect.getfullargspec(self.__init__)
        in_order_args = spec.args[1:]
        idx = in_order_args.index('eta')
        d["__args"] = d["__args"][:idx] + (self.eta,) + d["__args"][idx+1:]
        return d
