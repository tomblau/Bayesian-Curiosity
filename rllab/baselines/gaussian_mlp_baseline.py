import numpy as np
import inspect

from rllab.core.serializable import Serializable
from rllab.core.parameterized import Parameterized
from rllab.baselines.base import Baseline
from rllab.misc.overrides import overrides
from rllab.regressors.gaussian_mlp_regressor import GaussianMLPRegressor


class GaussianMLPBaseline(Baseline, Parameterized):

    def __init__(
            self,
            env_spec,
            subsample_factor=1.,
            num_seq_inputs=1,
            regressor_args=None,
    ):
        Serializable.quick_init(self, locals())
        super(GaussianMLPBaseline, self).__init__(env_spec)
        if regressor_args is None:
            regressor_args = dict()

        self._regressor = GaussianMLPRegressor(
            input_shape=(env_spec.observation_space.flat_dim * num_seq_inputs,),
            output_dim=1,
            name="vf",
            **regressor_args
        )
        self.subsample_factor = subsample_factor

    @overrides
    def fit(self, paths, indices=None):
        observations = np.concatenate([p["observations"] for p in paths])
        returns = np.concatenate([p["returns"] for p in paths])
        if indices is not None:
            observations = observations[indices]
            returns = returns[indices]
        if self.subsample_factor < 1:
            n_samples = returns.shape[0]
            inds = np.random.choice(
                n_samples, int(n_samples * self.subsample_factor), replace=False)
            observations = observations[inds]
            returns = returns[inds]
        self._regressor.fit(observations, returns.reshape((-1, 1)))

    @overrides
    def predict(self, path):
        return self._regressor.predict(path["observations"]).flatten()

    @overrides
    def get_param_values(self, **tags):
        return self._regressor.get_param_values(**tags)

    @overrides
    def set_param_values(self, flattened_params, **tags):
        self._regressor.set_param_values(flattened_params, **tags)

    def __getstate__(self):
        d = Parameterized.__getstate__(self)
        spec = inspect.getfullargspec(self.__init__)
        in_order_args = spec.args[1:]
        idx = in_order_args.index('subsample_factor')
        d["__args"] = d["__args"][:idx] + (self.subsample_factor,) + d["__args"][idx+1:]
        return d