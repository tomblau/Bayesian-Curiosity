# Bayesian Curiosity

This repository contains code and supplementary information for the paper *Bayesian Curiosity for Efficient Exploration in Reinforcement Learning*. Code is derived from the [RLLab library](https://github.com/rll/rllab).


# Requirements

This code was tested on an environment with a GPU and CUDA9.0. If these are not available, you can maybe get away with running the code in CPU-only mode or by running it in a conda env and not a docker, as described in the next section.


# Installation

This code was written to run inside of a docker and was only tested to run in such conditions. You may be able to get away with installing the conda environment in `docker/environment.yml` but we offer no guarantee that this will work. To build the docker image, go to the `docker` folder, and run
```
docker build -f gpu_Dockerfile -t rllab:latest .
```

Once the docker image is built, you can run it by executing `run.sh`. You may need to change the variable `GPU` to the id of whichever GPU you want the code to run on. You can find the ids of all your GPUs by running the command `nvidia-smi`

To run the robotics experiments, you will need to install V-REP 3.5 in the `V-REP` folder. Do not overwrite/delete the scenes in `V-REP/scenes`. Other versions of V-REP may not support the scenes provided in this repository.


# Implementation Directory

This section lists the filepaths to the specific files which implement important components described in the paper.

## Experiments

The files for running experiments of different groups are as follow:

**Bayesian Curiosity** experiments are in the folder `Bayesian-Curiosity/examples/visitation`. Cartesian robot experiments are in the files `lowd_reaching_ex.py` and `lowd_grasping_ex.py`. Visuomotor robot experiments are in the files `reaching_ex.py` and `grasping_ex.py`.

**VIME** experiments are in the folders `Bayesian-Curiosity/examples/reinforce` and `Bayesian-Curiosity/examples/trpo` respectively.

**RND** experiments are in the folder `Bayesian-Curiosity/examples/RND`

## Policies

**Bayesian Linear Regression** is implemented in `Bayesian-Curiosity/rllab/policies/components.py`

**Bayesian Curiosity** policies are implemented in the files `Bayesian-Curiosity/rllab/policies/visitation_deterministic_policy.py` and `Bayesian-Curiosity/rllab/policies/visitation_gaussian_policy.py`.

**RND** policies are implemented in the files `Bayesian-Curiosity/rllab/policies/rnd_deterministic_policy.py` and `Bayesian-Curiosity/rllab/policies/rnd_gaussian_policy.py`.

## Rewards

The curiosity rewards are added to state-action-reward tuples in the files implementing the appropriate algorithms.

**TRPO/REINFORCE** are implemented in `Bayesian-Curiosity/rllab/algos/batch_polopt.py`.

**DDPG** is implemented in `Bayesian-Curiosity/rllab/algos/ddpg.py`.

## Environments

Environment implementations live in the folder `Bayesian-Curiosity/rllab/envs`

**Mountaincar** is implemented in `Bayesian-Curiosity/rllab/envs/mountaincar_env.py`

**Pendulum** is implemented in `Bayesian-Curiosity/rllab/envs/pendulum_env.py`

**Acrobot** is implemented in `Bayesian-Curiosity/rllab/envs/acrobot_env.py`

**Cartpole Swingup** is implemented in `Bayesian-Curiosity/rllab/envs/box2d/cartpole_swingup_env.py`

**Reaching** tasks are implemented in `Bayesian-Curiosity/rllab/envs/vrep/jaco_env.py`

**Grasping** tasks are implemented in `Bayesian-Curiosity/rllab/envs/vrep/grasping/jaco_env.py`


# Running Experiments

The classic control experiments can be run by simply executing the python files in `examples`. E.G. to run the mountaincar TRPO experiment, simply use the command `python examples/visitation/mountaincar_visitation_trpo.py`. You can change the parameters of an experiment from the relevant python file, including changing the model. See the readme in the `examples` folder for further details.

To run an experiment without Bayesian Curiosity rewards, simply add the line `policy.eta = 0.` to the python code of the relevant experiment.

For a more thorough description of how to reproduce the results, see `methodology.md`
