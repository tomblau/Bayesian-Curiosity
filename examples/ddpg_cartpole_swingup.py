from rllab.misc.instrument import run_experiment_lite


def run_task(*_):
    from rllab.algos.ddpg import DDPG
    from rllab.envs.box2d.cartpole_swingup_env import CartpoleSwingupEnv
    from rllab.envs.normalized_env import normalize
    from rllab.policies.deterministic_mlp_policy import DeterministicMLPPolicy
    from rllab.exploration_strategies.ou_strategy import OUStrategy
    from rllab.q_functions.continuous_mlp_q_function import ContinuousMLPQFunction

    
    env = normalize(CartpoleSwingupEnv())
    
    policy = DeterministicMLPPolicy(
        env_spec=env.spec,
        # The neural network policy should have two hidden layers, each with 32 hidden units.
        hidden_sizes=(32, 32)
    )
    
    es = OUStrategy(env_spec=env.spec)

    qf = ContinuousMLPQFunction(env_spec=env.spec)

    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=100,
        max_path_length=100,
        epoch_length=4000,
        min_pool_size=10000,
        n_epochs=1010,
        discount=0.99,
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=1,
)
