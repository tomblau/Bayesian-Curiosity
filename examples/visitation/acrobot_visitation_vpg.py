from rllab.misc.instrument import run_experiment_lite


seeds = [372102, 403627, 171887, 3573, 676794, 703736, 443044, 678579, 896001, 288864]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.vpg import VPG
    from rllab.envs.acrobot_env import AcrobotEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import VisitationBatchSampler
    import joblib


    env = normalize(AcrobotEnv(seed=seed), normalize_obs=True)
    
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = VPG(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4*env.horizon,
        max_path_length=env.horizon,
        n_itr=501,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=500,
    seed=seed,
)
