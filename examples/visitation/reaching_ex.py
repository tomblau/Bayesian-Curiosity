from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite

seeds = [49445, 424522, 984189, 922977, 35796, 947600, 801314, 624999, 259907, 658686]
exp_paths = ['experiment_2018_10_22_04_34_59_0001', 'experiment_2018_10_25_04_10_50_0001', 'experiment_2018_10_25_05_17_59_0001', 'experiment_2018_10_26_02_37_56_0001', 'experiment_2018_10_26_02_39_01_0001']
exp_id = 1
seed = seeds[exp_id - 1]
exp_path = exp_paths[exp_id - 1]

def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.algos.batch_polopt import VisitationBatchSampler
    from rllab.envs.vrep.jaco_env import JacoEnv
    import joblib
    
    env = JacoEnv()
    obs_shape = (128, 128, 4)
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    
    print("loading model from %s" %init_weights)
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=100,
        max_path_length=25,
        n_itr=4010,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    seed=seed,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    # plot=True,
)
