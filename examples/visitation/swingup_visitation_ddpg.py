from rllab.misc.instrument import run_experiment_lite


seeds = [31806, 561228, 796366, 53610, 346821, 637675, 422602, 235697, 521484, 916697]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.ddpg import DDPG
    from rllab.envs.box2d.cartpole_swingup_env import CartpoleSwingupEnv
    from rllab.envs.normalized_env import normalize
    from rllab.q_functions.continuous_mlp_q_function import ContinuousMLPQFunction
    import joblib

    
    env = normalize(CartpoleSwingupEnv())

    #replace this with a path to an algo object
    init_weights="path/to/algo"
    data = joblib.load(init_weights)
    policy = data["policy"]
    es = data["es"]
    qf = data["qf"]
    

    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=100,
        max_path_length=env.horizon,
        epoch_length=4000,
        min_pool_size=10000,
        n_epochs=1010,
        discount=0.99,
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=seed,
)
