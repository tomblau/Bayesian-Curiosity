from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite

seeds = [31806, 561228, 796366, 53610, 346821, 637675, 422602, 235697, 521484, 916697]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.vpg import VPG
    from rllab.envs.box2d.cartpole_swingup_env import CartpoleSwingupEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import RNDBatchSampler
    import joblib


    env = normalize(CartpoleSwingupEnv(), normalize_obs=True)
    init_weights = None
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = VPG(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=40*env.horizon,
        max_path_length=env.horizon,
        n_itr=1010,
        discount=0.99,
        step_size=0.01,
        sampler_cls=RNDBatchSampler,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=seed,
)
