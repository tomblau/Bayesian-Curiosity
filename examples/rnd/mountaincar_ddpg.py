from rllab.misc.instrument import run_experiment_lite


seeds = [604624, 729325, 414791, 855535, 792336, 619888, 691673, 789430, 342476, 551625]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.ddpg import DDPG
    from rllab.envs.mountaincar_env import MountainCarEnv
    from rllab.envs.normalized_env import normalize
    import joblib


    env = normalize(MountainCarEnv(seed=seed), normalize_obs=True)
    
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    data = joblib.load(init_weights)
    policy = data["policy"]
    es = data["es"]
    qf = data["qf"]
    

    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=100,
        max_path_length=env.horizon,
        epoch_length=env.horizon,
        min_pool_size=1000,
        n_epochs=110,
        discount=0.99,
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=100,
    seed=seed,
)
