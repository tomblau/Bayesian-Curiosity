from rllab.misc.instrument import run_experiment_lite


seeds = [441896, 158560, 981798, 223679, 993738, 36321, 562168, 795055, 953569, 847874]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.ddpg import DDPG
    from rllab.envs.pendulum_env import PendulumEnv
    from rllab.envs.normalized_env import normalize
    import joblib


    env = normalize(PendulumEnv(seed=seed))
    
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    data = joblib.load(init_weights)
    policy = data["policy"]
    es = data["es"]
    qf = data["qf"]
    
    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=100,
        max_path_length=env.horizon,
        epoch_length=40*env.horizon,
        min_pool_size=10000,
        n_epochs=1010,
        discount=0.99,
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=500,
    seed=seed,
)
