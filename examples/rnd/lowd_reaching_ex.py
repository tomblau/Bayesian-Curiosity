from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite

seeds = [711003, 808001, 626291, 922281, 663247, 951441, 863258, 842737, 900360, 399605]
exp_id = 5
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.algos.batch_polopt import RNDBatchSampler
    from rllab.envs.vrep.jaco_env import LowDJacoEnv
    import joblib
    
    env = LowDJacoEnv()
    action_dim = env.action_space.flat_dim
    obs_shape = env.observation_space.shape
    init_weights = None
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=100,
        max_path_length=25,
        n_itr=4010,
        discount=0.99,
        step_size=0.01,
        sampler_cls=RNDBatchSampler,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    seed=seed,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    # plot=True,
)
