from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite

seeds = [604624, 729325, 414791, 855535, 792336, 619888, 691673, 789430, 342476, 551625]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.envs.mountaincar_env import MountainCarEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import RNDBatchSampler
    import joblib


    env = normalize(MountainCarEnv(seed=seed),normalize_obs=True)
    init_weights = None
    #replace this with a path to an algo object
    init_weights="path/to/algo"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = TRPO(
         env=env,
        policy=policy,
        baseline=baseline,
        batch_size=env.horizon,
        max_path_length=env.horizon,
        n_itr=110,
        discount=0.99,
        step_size=0.01,
        sampler_cls=RNDBatchSampler,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    seed=seed,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=100,
)
