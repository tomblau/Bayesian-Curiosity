from rllab.misc.instrument import run_experiment_lite

seed=1
def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
    from rllab.envs.gym_env import GymEnv
    from rllab.envs.normalized_env import normalize
    from rllab.envs.mountaincar_env import MountainCarEnv
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    import joblib
    # Please note that different environments with different action spaces may require different
    # policies. For example with a Box action space, a GaussianMLPPolicy works, but for a Discrete
    # action space may need to use a CategoricalMLPPolicy (see the trpo_gym_cartpole.py example)
    env = normalize(MountainCarEnv(seed=seed))
    init_weights = None
    init_weights = "/rllab/data/local/experiment/experiment_2019_01_30_03_56_53_0001/itr_0b.pkl"
    if init_weights is None:
        policy = GaussianMLPPolicy(
            env_spec=env.spec,
            # The neural network policy should have two hidden layers, each with 32 hidden units.
            hidden_sizes=(32, 32),
            init_std=0.3
        )
        baseline = LinearFeatureBaseline(env_spec=env.spec)
    else:
        data = joblib.load(init_weights)
        policy = data["policy"]
        baseline = data["baseline"]


    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4*env.horizon,
        max_path_length=env.horizon,
        n_itr=501,
        discount=0.99,
        step_size=0.01,
    )
    algo.train()


run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=100,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    seed=seed,
)
