from rllab.algos.ddpg import DDPG
from rllab.envs.vrep.jaco_env import JacoEnv
from rllab.envs.normalized_env import normalize
from rllab.misc.instrument import run_experiment_lite
from rllab.exploration_strategies.dropout_strategy import DropoutStrategy
from rllab.policies.deterministic_mlp_policy import DeterministicMLPPolicy
from rllab.core.network import AltRoboNetwork as RoboNetwork
from rllab.q_functions.continuous_mlp_q_function import ContinuousMLPQFunction
import lasagne.nonlinearities as NL
from rllab.core.lasagne_helpers import ScaledSigmoid
import joblib


def run_task(*_):
    env = normalize(JacoEnv())
    action_dim = env.action_space.flat_dim
    obs_shape = (128, 128, 4)
    init_weights = None
    init_weights = "/rllab/data/local/experiment/ddpg/trained_det.pkl"

    if init_weights:
        data = joblib.load(init_weights)
        policy = data['policy']
        qf = data['qf']
    else:
        p_network = RoboNetwork(
                         input_shape=obs_shape,
                         output_dim=action_dim,
                         hidden_sizes=(256, 256, 64),
                         pool_sizes=(2, 2, 2),
                         conv_filters=(32, 64, 64),
                         conv_filter_sizes=(6, 6, 4),
                         conv_strides=(2, 2, 2),
                         conv_pads=(1, 1, 1),
                         hidden_nonlinearity=NL.tanh,
                         output_nonlinearity=NL.tanh,
                    )
        policy = DeterministicMLPPolicy(
            env_spec=env.spec,
            network=p_network
        )

        o_network = RoboNetwork(
                         input_shape=obs_shape,
                         output_dim=128,
                         hidden_sizes=(256, 256),
                         pool_sizes=(2, 2, 2),
                         conv_filters=(32, 64, 64),
                         conv_filter_sizes=(6, 6, 4),
                         conv_strides=(2, 2, 2),
                         conv_pads=(1, 1, 1),
                         hidden_nonlinearity=NL.tanh,
                         output_nonlinearity=NL.tanh,
        )
        qf = ContinuousMLPQFunction(
                         env_spec=env.spec,
                         obs_network=o_network,
                         hidden_sizes=(128, 64),
                         action_merge_layer=0
        )

    es = DropoutStrategy(env_spec=env.spec)



    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=32,
        max_path_length=25,
        epoch_length=200,
        min_pool_size=2000,
        replay_pool_size=10000,
        n_epochs=100,
        discount=0.99,
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        eval_samples=100
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="last",
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    seed=1,
    # plot=True,
)
