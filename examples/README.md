# Running Experiments

You can the experiments by running the appropriate python file in this folder or sub-folders. Names should be self-explanatory. Note that you have to provide a path to a .pkl file that contains an appropriate model. A model must contain an entry for 'policy' implementing either the `VisitationGaussianPolicy` class (for TRPO/REINFORCE) or the `VisitationDeterministicPolicy` class (for DDPG). These classes are found in `Bayesian-Curiosity/rllab/policies`. The .pkl file must also contain an entry 'baseline' implementing the `Baseline` class (TRPO/REINFORCE) found in `Bayesian-Curiosity/rllab/baselines` or entries for 'es' and 'qf' implementing the `ExplorationStrategy` and `QFunction` classes respectively (DDPG).

Note that vrep experiments must have the appropriate vrep scene running or they will fail. For convenience, the `scripts` folder contains shell scripts to run the experiments in a single command. These scripts clean up after themselves using a kill command, so DO NOT RUN THEM OUTSIDE A DOCKER unless you want to reboot your machine.

Further, vrep experiments require a monitor. If you're running a headless machine, you need to first run the command `./monitor.sh` in the root folder to start an x-frame buffer.
